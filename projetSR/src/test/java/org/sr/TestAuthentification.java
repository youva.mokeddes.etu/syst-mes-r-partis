package org.sr;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

public class TestAuthentification {
	   
      @Test(expected = IOException.class)
      public void testAuthentificationWithWrongUser() throws IOException {
    	  Authentification auth = new Authentification("anonymous12","anonymous","ftp.ubuntu.com",21);
    	  //auth.connect();
    	  auth.sendCommande(("USER " + auth.getUser()));
    	  String rep = auth.receive();
    	  
      }
	   
      @Test(expected = IOException.class)
      public void testAuthentificationWithWrongPass() throws IOException {
    	  Authentification auth = new Authentification("anonymous","anonymous12","ftp.ubuntu.com",21);
    	  //auth.connect();
    	  auth.sendCommande(("PASS " + auth.getUser()));
    	  String rep = auth.receive();  
      }	  
      
      @Test
      public void testAuthentificationWithGoodPassAndLogin() throws IOException{
    	  Authentification auth = new Authentification("anonymous","anonymous","ftp.ubuntu.com",21);
    	  auth.run();
    	  auth.sendCommande(("PASS " +auth.getUser()));
    	  String rep = auth.receive(); 
    	  assertTrue(rep.startsWith("230"));

      }
	  
      @Test
      public void testCWDWithGoodFolder() throws IOException{
    	  Authentification auth = new Authentification("anonymous","anonymous","ftp.ubuntu.com",21);
    	  auth.run();
    	  RequestFTP rq = new RequestFTP(auth);
    	  assertTrue(rq.cwd("cdimage"));

      }     
      
      @Test
      public void testCWDWithWrongFolder() throws IOException{
    	  Authentification auth = new Authentification("anonymous","anonymous","ftp.ubuntu.com",21);
    	  auth.run();
    	  RequestFTP rq = new RequestFTP(auth);
    	  assertFalse(rq.cwd("titi"));

      }
      
      @Test
      public void testDisplayCurrentDirectory() throws IOException{
    	  Authentification auth = new Authentification("anonymous","anonymous","ftp.ubuntu.com",21);
    	  auth.run();
    	  RequestFTP rq = new RequestFTP(auth);
    	  rq.displayCurrentDirectory();

      }      
      
      @Test(expected = IOException.class)
      public void testDisplayCurrentDirectoryWithNoConnection() throws IOException{
    	  Authentification auth = new Authentification("anonymous","anonymous","ftp.ubuntu.com",21);
    	  //auth.run();
    	  RequestFTP rq = new RequestFTP(auth);
    	  rq.displayCurrentDirectory();

      }    
            
       
      
      
      
      
      
      
}
