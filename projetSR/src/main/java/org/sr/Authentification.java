package org.sr;

import java.io.*;
import java.net.Socket;
import java.util.StringTokenizer;

/**
 * la class Authentification nous permet de nous connecter au serveur
 * @author mokeddes
 *
 */
public class Authentification {
    private Socket mysocket = null;
    private BufferedReader myBufferedReader = null;
    private BufferedWriter myBufferedWriter = null;
    private String adress;
    private int port;
    private String user;
    private String passWord;
    
    /**
     * @param user utilisateur
     * @param password mot de passe
     * @param adress l'hote
     * @param port port
     */
	public Authentification (String user,String password,String adress,int port) {
	
		this.user = user;
		this.passWord = password;
		this.port = port;
		this.adress = adress;
		
	}
	/**
	 * 
	 * @return soocket
	 */
	
	public Socket getSocket() {
		return this.mysocket;
	}
	/**
	 * 
	 * @return user
	 */
	public String getUser() {
		return this.user;
	}  
	/**
	 * 
	 * @return reset the socket
	 */
  	public Socket resetSocket() {
		return this.mysocket = null;
	}
  	/**
  	 * 
  	 * @return myBufferedReader
  	 */
	public BufferedReader getReader() {
		return this.myBufferedReader;
	}
	/**
	 * 
	 * @return myBufferedWriter
	 */
	public BufferedWriter getwriter() {
		return this.myBufferedWriter;
	}
	
	/**
	 * cette fonction nous permet de nous connecter au serveur en utilisant PASS et USER
	 * @throws IOException
	 */
	public void run() throws IOException {
    	if (mysocket != null) {
            throw new IOException("erreur FTP");
       }
    
        mysocket = new Socket(adress, port);
        myBufferedReader = new BufferedReader(new InputStreamReader(mysocket.getInputStream()));
        myBufferedWriter = new BufferedWriter(new OutputStreamWriter(mysocket.getOutputStream()));

        String reponse = receive();
        if (!reponse.startsWith("220 ")) throw new IOException("erreur Serveur" + reponse);

        sendCommande("USER " + user);
        reponse = receive();
       
        if (!reponse.startsWith("331 ")) throw new IOException(reponse);

        sendCommande("PASS " + passWord);
        reponse = receive();
        if (!reponse.startsWith("230 ")) throw new IOException(reponse);
    }
    
	/**
	 * 
	 * @param line le string qu'on veut envoyer au serveur
	 * @throws IOException
	 */
	public void sendCommande(String line) throws IOException {
        if (mysocket == null) {
            throw new IOException("erreur ftp");
        }
        try {
        	myBufferedWriter.write(line + "\r\n");
        	myBufferedWriter.flush();
        } catch (IOException e) {
            mysocket = null;
            throw e;
        }
    }
	/**
	 * 
	 * @return la réponse du serveur 
	 * @throws IOException
	 */
    public String receive() throws IOException {
        String line = myBufferedReader.readLine();
        return line;
    }

    
    
}
