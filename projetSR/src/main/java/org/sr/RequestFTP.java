package org.sr;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.StringTokenizer;
/**
 * 
 * @author mokeddes
 *
 */

public class RequestFTP {
    Authentification auth;
   /**
    *  
    * @param auth
    */
    public RequestFTP(Authentification auth) {
    	this.auth = auth;
    	
    }

    /**
     * 
     * @return Afficher le répertoire de travail actuel sur la machine distante
     * @throws IOException
     */

    public String pwd() throws IOException {
        String folder = null;
        this.auth.sendCommande("PWD");
        String retour = this.auth.receive();
        if (retour.startsWith("257 ")) {
            int a = retour.indexOf('\"');
            int b = retour.indexOf('\"', a + 1);
            if (b > 0) {
                folder = retour.substring(a + 1, b);
            }
        }
        return folder;
    }
	
    
    
    
    /**
     * cwd nous permet de rentrer dans le dossier (dir) donner en paramétre
     * @param dir
     * @return
     * @throws IOException
     */
    public boolean cwd(String dir) throws IOException {
    	this.auth.sendCommande("CWD " + dir);
    	String retour = null;
        try {
        	  retour = this.auth.receive();
        } catch (IOException e) {

        }
        return (retour.startsWith("250 "));
       
    }
    
    
    
    
    /**
     * id_dir verifie si elt est un dossier
     * @param elt 
     * @return
     */
    public boolean is_dir(String elt) {
    	return elt.startsWith("d");
    }
    
    
    
    /**
     * cette fonction nous affiche le contenue du répertoire où nous nous trouvons 
     * @return
     * @throws IOException
     */
    public  String displayCurrentDirectory() throws IOException {
    	this.auth.sendCommande("PASV");
        String retour = this.auth.receive();
       
        if (!retour.startsWith("227 ")) {
            throw new IOException("impossible de se connecter en mode passif" + retour);
        }

        String ip = "nothing";
        int port = 1234;
        
        int ParOuvrante = retour.indexOf('(');
        int ParFermante = retour.indexOf(')', ParOuvrante + 1);
        if (ParFermante > 0) {
            String donnéesR = retour.substring(ParOuvrante + 1, ParFermante);
            StringTokenizer token = new StringTokenizer(donnéesR, ",");
            try {
                ip = token.nextToken() + "." + token.nextToken() + "."+ token.nextToken() + "." + token.nextToken();
               
                port = Integer.parseInt(token.nextToken())*256 + Integer.parseInt(token.nextToken());
               
               
            } catch (Exception e) {
                throw new IOException("échec de connexion" + retour);
            }
        }
        
		Socket Socket2 = new Socket(ip, port);
		this.auth.sendCommande("LIST");
        retour = this.auth.receive();
        if (retour.startsWith("425 ")) throw new IOException("pour utiliser LIST il faut créer un 2eme socket en utilisant PORT ou PASV");

        BufferedReader input = new BufferedReader(new InputStreamReader(Socket2.getInputStream()));
        BufferedReader br = new BufferedReader(input);
        String files = new String(), nextLine = input.readLine();
        while(nextLine != null){
            files = (new StringBuilder()).append(files).append(nextLine).append("\n").toString();
            nextLine = input.readLine();
        }
        input.close();
        br.close();
        return files;
    }
    
    /**
     * cette fonction rentre dans dir et nous affiche son contenue
     * @param dir
     * @return
     * @throws IOException
     */
    public String display(String dir) throws IOException {
    	String current_dir = this.pwd();
    	this.cwd(dir);
        String response = this.displayCurrentDirectory();
        this.auth.resetSocket();
        try {
			this.auth.run();
	    	this.cwd(current_dir);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        this.cwd(current_dir);
        return response;
    }
    
   /**
    * cette fonction nous permet de nous deconecter du serveur
    * @throws IOException
    */
    public  void deconnexion() throws IOException {
        try {
            this.auth.sendCommande("QUIT");
        } finally {
            this.auth.getSocket().close();
        }
    }
   
    /**
     * la fonction tree nous affiche l'arbre d'un serveur
     * @param doc
     * @param req
     * @param Prefix
     * @throws IOException
     */
    
    public  void tree(String doc,RequestFTP req,String Prefix) throws IOException {
        StringTokenizer tokenizer = new StringTokenizer(doc,"\n");
        while (tokenizer.hasMoreTokens()) {
        	String tok = tokenizer.nextToken();
            System.out.println(Prefix+tok.substring(55)); 
            if(this.is_dir(tok)) {
               req.cwd(tok.substring(55).trim());
               tree(req.display(req.pwd()),req,Prefix+"  |__");
                  
            }}
        }   
 
    
 
    
    public static void main(String[] args) throws IOException {
    	Authentification authentification;
        if (args.length == 4) {
             authentification = new Authentification(args[0],args[1],args[2],Integer.parseInt(args[3]));
             try {
             	authentification.run();
     		} catch (Exception e) {
     			e.printStackTrace();
     		}
              
             RequestFTP requestFtp = new RequestFTP(authentification);
             System.out.println(".");
             requestFtp.tree(requestFtp.display(requestFtp.pwd()),requestFtp,"");
             requestFtp.deconnexion();             
             
        }
        if (args.length == 1) {
        	authentification = new Authentification("anonymous","anonymous",args[0],21);
            try {
             	authentification.run();
     		} catch (Exception e) {
     			e.printStackTrace();
     		}
              
             RequestFTP requestFtp = new RequestFTP(authentification);
             System.out.println(".");
             requestFtp.tree(requestFtp.display(requestFtp.pwd()),requestFtp,"");
             requestFtp.deconnexion(); 
        	
        }
     
    }
}


